const inputs = document.querySelectorAll('.register_section .register_section_form input')
const submitButton = document.querySelector('.register_form_submit')

console.log(inputs)
/* console.log(submitButton) */
const passRegex = {
  name: false,
  username: false,
  password: false,
  email: false,
  agree: false
}

const regex = {
  name: /^[a-zA-Z\s]{8,30}$/,
  username: /^[\w\d]{6,30}$/,
  email: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  password: /^(?=.*[\w])(?=.*[\d])(?=.*[!\*\?\^\$&#@_\-])[\w\d!\*\?\^\$&#@_\-]{8,30}$/
}
const newUser = {
  name: '',
  username: '',
  email: '',
  password: ''
}
// verifing data from email,pass,usermane, name ussing regex object
const validateData = (e) => {
  /* console.log(e.target.value, e.target.name)
  console.log(regex[e.target.name]) */
  /* console.log(e) */
  /* if (e.target.name === 'email') return */
  if (regex[e.target.name].test(e.target.value)) {
    newUser[e.target.name] = e.target.value
    passRegex[e.target.name] = true
    document.querySelector('#' + e.target.name).setAttribute('class', 'good_data_input register_from_input')
  } else {
    console.log('bad data')
    document.querySelector('#' + e.target.name).setAttribute('class', 'bad_data_input register_from_input')
    passRegex[e.target.name] = false
  }
  /* console.log(passRegex) */
}
// add keyup listener to elements whit type=text,password,email
inputs.forEach((input) => {
  input.addEventListener('keyup', validateData)
})
// add click event to element whit type=checbox
const hadleCheckbox = (e) => {
  console.log(e.target.checked)
  passRegex.agree = e.target.checked
}
inputs[4].addEventListener('click', hadleCheckbox)
// submit function
const verifyInfo = (e) => {
  e.preventDefault()
  const estate = [passRegex.name, passRegex.username, passRegex.password, passRegex.agree]
  console.log(estate.every(x => x === true))
  if (estate.every(x => x === true)) {
    console.log('enviamos los datos')
  } else {
    console.log('revisa la informacion o acepta las condiciones')
  }
  console.log(newUser)
}

submitButton.addEventListener('click', verifyInfo)
